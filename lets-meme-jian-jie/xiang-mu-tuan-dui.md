# 项目团队

创始团队成员出身自阿里、字节、腾讯等大厂，囊括了伯克利、UVA、浙大等高校的研究生、博士生。遍布在杭州、香港以及洛杉矶。团队创始人深耕Crypto领域八年，有非常丰富的web3行业经验和成功创业经历。团队95后为主，氛围好，成员充满活力和创业激情。&#x20;



今年创立了TurtleCase Gang的NFT品牌，是华语NFT圈的代表之一。在业内积聚了丰富的资源生态。同时与海外社区有很强的连接，项目处在早期阶段，前景巨大。



<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption><p>Turtlecase Gang</p></figcaption></figure>
