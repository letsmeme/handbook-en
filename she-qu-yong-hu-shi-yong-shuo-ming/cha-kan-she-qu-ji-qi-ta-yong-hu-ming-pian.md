# 查看社区及其他用户名片

<figure><img src="https://xyp4jzgb7d.feishu.cn/space/api/box/stream/download/asynccode/?code=ZjcxYmE5MjA2NmFhZjI3YjMxMzZlMWUyOGJjZWQ1NDdfdEFieGlORTBHemd0UDBPZFNram5JeXI0YzRLaHRVcEVfVG9rZW46Ym94Y252NjB3ejNXT1NOMkNjMHN2VE11ZHNlXzE2NzAzMzk0NDA6MTY3MDM0MzA0MF9WNA" alt=""><figcaption><p>在社区官推的主页可以查看关于社区的各类信息</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (7).png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

#### 1）社区基础信息

社区基础信息包含：1、社区logo2、社区名称3、社区简介4、社区所对应的各类链接：官网、opensea、Discord、Twitterscan、element等等\


#### 2）Members

可以在成员处查看当下使用插件的社区用户总量及今日新增人数点击Members List即可在弹窗可查看社区全体成员，并可一键Follow All并且可在此弹窗页面查看社区内所有用户的社区积分及排名\


#### 3）Notification

可在此模块查看社区通知\


#### 4）社区任务

同Home一样，可直接完成任务。\


#### 5）其他用户名片展示

1、可在Members List中点击任一用户头像进行其他用户推特主页的跳转及其名片的查看2、直接在推特上进行用户搜素或点击，即可在插件位置进行其他用户名片的查看
