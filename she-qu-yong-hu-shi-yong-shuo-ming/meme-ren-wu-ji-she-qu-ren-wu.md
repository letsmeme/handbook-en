# MeMe任务及社区任务

<figure><img src="https://xyp4jzgb7d.feishu.cn/space/api/box/stream/download/asynccode/?code=YzQ3NWEzNGY4OThjMTc5NTExZWNlMDc4NDY3OGI3NzJfTEN3N2pyUDBBNHF3aGRVNHdYSDJjWnNGTGZRZDNXdDRfVG9rZW46Ym94Y25LY3F3clcxRDNpVFVOOFZYeElrQkFkXzE2NzAzMzkyMTY6MTY3MDM0MjgxNl9WNA" alt=""><figcaption><p>在完成推特及钱包的连接后，我们可以在推特的Home页面，查看我们所处的社区任务及MeMe任务</p></figcaption></figure>





<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

#### 1）社区任务

社区任务为用户所处的社区自行发布的私域任务，目标群体仅为社区内部的所有用户。设置的奖励为该社区的内部积分，该积分有和作用及其是否与通用代币MeMe挂钩，或与其他Token挂钩为社区自行设置。任何完成任务的用户都将获得本社区的积分作为奖励。

#### 2）MeMe任务

MeMe任务为Let's MeMe的公域任务，为Let's MeMe官方发布的任务，与项目方使用代币MeMe购买公域流量进行发布。用户完成MeMe任务即可获得任务配置的MeMe代币。

#### 3）社区信息

点击Community Task中社区的头像，即可切换跳转到该社区的主页

#### 4）如何完成任务及提交

当下任务主要为四大类条件：

1. 关注，需要用户去关注某一推特账号用户
2. 点赞，需要用户去点赞某一条推特推文
3. 转推，需要用户去转推某一条推特推文
4. 评论，需要用户去评论某一条推特推文

任一任务目前都是由以上四大类条件组合形成的\
当用户完成某一任务后，即可点击对应任务的Submit按钮，当任务显示为Pinding Verificatoin即为提交成功，等待审核。审核完成后，如按照要求完成任务的，即可获得该任务相对应的奖励。\


#### 5）签到任务

TCG社区目前独有签到任务，每日都可完成一次，按照NFT持仓数量进行相对应的积分奖励
