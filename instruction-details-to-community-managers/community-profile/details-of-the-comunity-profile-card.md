# Details of the Comunity Profile Card

1.  **General Community Info**

    The upper portion of the community profile card contains four modules: logo, nickname, profile, and link.The logo, nickname, profile, and website link are all generated automatically with the contract address provided by the project party. These are incorporated from the Opensea page links. In order to edit them, the community must change the relevant information available on Opensea.
2.  **Member List**

    A members section similar to previously shown members sections.
3.  **NotificationCommunity notice board.**&#x20;

    Click manage to edit and publish community announcements in the pop-up window.
4.  **TaskView and manage tasks for this community.**

    Click manage to open the task management pop-up window.\
