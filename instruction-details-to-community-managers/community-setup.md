# Community Setup

We currently have bound the main Twitter accounts of more than 2000+ projects to their NFT contract addresses. The project party simply needs to download the plug-in, log in to its official account, and connect any wallet to gain admin access.

Unbound communities can contact us via our official Twitter: [https://twitter.com/Letusmeme3](https://twitter.com/Letusmeme3), or DC server: [https://discord.gg/8h344HNWcu](https://discord.gg/8h344HNWcu). and provides us with the following to bind their communities:

1. &#x20;**** Official Twitter link
2. &#x20;Contract address\
