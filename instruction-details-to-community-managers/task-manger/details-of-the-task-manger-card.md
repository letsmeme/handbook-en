# Details of the Task Manger Card

**Ongoing Tasks displays a list of current tasks.**\
Within each task, information relevant to the task is displayed including:

1. Follow/reply/retweet/like account or tweet link
2. How many users completed the task
3. Reward given for this taskAfter clicking the End task button, the corresponding task will undergo verification and conclude.\


**New Task allows you to publish new tasks.**

Currently, all project parties (except the Let's MeMe project party) cannot publish platform-wide tasks (ie. public tasks). The community task is a private task only for all of the community users.Each task allows for 1\~N conditions, of which there are four categories: follow, like, retweet, comment. In the far right input box, enter the link of either the Twitter account that needs to be followed or the tweet to be liked, commented, or retweeted. Enter the reward amount for this task next to Points. Click confirm to publish the completed task form.\


