# Table of contents

* [💡 Introduction](README.md)
* [🪐 Instruction Details to Users](instruction-details-to-users/README.md)
  * [Download and Update the Extension](instruction-details-to-users/download-and-update-the-extension.md)
  * [Setup Profile Card](instruction-details-to-users/setup-profile-card/README.md)
    * [Details of the Personal Card](instruction-details-to-users/setup-profile-card/details-of-the-personal-card.md)
  * [MeMe and Community Tasks](instruction-details-to-users/meme-and-community-tasks/README.md)
    * [Details of the Task Card](instruction-details-to-users/meme-and-community-tasks/details-of-the-task-card.md)
  * [Notifications](instruction-details-to-users/notifications.md)
  * [Other Community and Profile Cards](instruction-details-to-users/other-community-and-profile-cards/README.md)
    * [Details of the Community Card](instruction-details-to-users/other-community-and-profile-cards/details-of-the-community-card.md)
* [🛰 Instruction Details to Community Managers](instruction-details-to-community-managers/README.md)
  * [Community Setup](instruction-details-to-community-managers/community-setup.md)
  * [Community  Profile](instruction-details-to-community-managers/community-profile/README.md)
    * [Details of the Comunity Profile Card](instruction-details-to-community-managers/community-profile/details-of-the-comunity-profile-card.md)
  * [Task Manger](instruction-details-to-community-managers/task-manger/README.md)
    * [Details of the Task Manger Card](instruction-details-to-community-managers/task-manger/details-of-the-task-manger-card.md)
* [☎ Q\&A](q-and-a.md)

## 🥂 Links

* [Whitepaper](https://app.gitbook.com/o/AZwd83ochQVfXudXDc6x/s/9vQDOSPFLQlPki6jBl0T/)
* [User Guide (Chinese)](https://app.gitbook.com/o/AZwd83ochQVfXudXDc6x/s/VpA3ejsSGzFvCLRsiu6U/)
* [Homepage](https://letsmeme.xyz)
