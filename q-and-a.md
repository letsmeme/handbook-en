# ☎ Q\&A

## **MeMe Points and Other Tokens**

Currently, Let's MeMe rewards users for completing world tasks with MeMe points. Subsequent MeMe points will be corresponded 1-1 with MeMe tokens. The MeMe token will become the standard token within the future MeMe-traffic universe. The token will be used to purchase real-user traffic and to exchange for various other tokens in all of Let’s MeMe’s products.

For example, in the subsequent versions of the Let's MeMe plug-in, communities will have to use MeMe to publish platform-wide tasks and conduct public giveaways.



## **TCG’s Relationship with Let’sMeme**

Currently, Turtlecase Gang is the only project whose community points are directly bound to MeMe, that is, TCG community users can obtain MeMe points by completing community tasks. More NFTs held correspond to being able to create more wallet-accounts, complete more tasks, and gain more rewards.

****
