# 社区信息

## 一：社区基础信息

名片的上方是社区信息部分，包含了logo、昵称、简介、链接四个模块 其中logo、昵称、简介、链接都是由项目方提供的合约地址，智能读取生成的。 logo、昵称、简介是与opensea相关联，如需更改，变更opensea相关信息即可。



## 二：Member List

点击Members List即可在弹窗可查看社区全体成员，并可一键Follow All&#x20;

并且可在此弹窗页面查看社区内所有用户的社区积分及排名



## 三：Change point

<img src="https://xyp4jzgb7d.feishu.cn/space/api/box/stream/download/asynccode/?code=OTU0OGFkMDg4NmIzOGYyMWIwNjI0ZjYwOWY1YjA4MmRfRFd2UDRCc3VnTWJvZTBZd3daNmIycm4zQjVGMTRzOE9fVG9rZW46Ym94Y25Ec1J0S0xuMklhWXlKMjV6MDhIWkVnXzE2NzAzMzg5Mjc6MTY3MDM0MjUyN19WNA" alt="" data-size="original"><img src="https://xyp4jzgb7d.feishu.cn/space/api/box/stream/download/asynccode/?code=ZDE1NWIxMzcwMjgwNmNjOWUxYzM0NzdiM2MyNmQ5ZTlfWHJ4c2x0bFR1UXJsNW41Q096MUFwTXB6R2pKaXJOdXlfVG9rZW46Ym94Y25ZUHdyTVJ4emw1dW5EbFVaeXZJTTlnXzE2NzAzMzg5MzY6MTY3MDM0MjUzNl9WNA" alt="" data-size="original">

点击Change point 弹出积分管理页面&#x20;

1. 进行积分手动操作，可以直接在下面操作面板找到对应需要操作的对象或者在搜索栏进行用户搜索。 找到对应用户后，输入需要变化的积分，再点击+/-号选择进行增减积分。最后点击Confirm进行确认&#x20;
2. 进行表格批量操作，在积分管理页面点击 Bulk update，在Bulk update页面，可以点击template下载模板，按照模板导入用户钱包地址及对应积分。将表格点击upload上传后，选择Increase（在原定积分上加入表格数据）/Coverage（用表格数据覆盖原有数据）后，点击Confirm进行确认





## 四：Notification

社区公告栏

点击manage，可在弹窗出进行社区公告的内部编辑与发布\


## 五：Task

进行本社区任务的查看及管理点击manage弹出任务管理弹窗Ongoing Task处可以进行已发布任务的查看及任务结算任务信息处可以查看过往发布任务的各类情况，信息包含：

1. 关注/回复/转推/点赞的目标及目标推文
2. 有多少用户完成了该任务
3. 该任务的奖励设置

点击End task按钮后，对应的任务会进行任务结算

\
New Task：可以进行新任务的发布

当前阶段所有项目方（除去Let's MeMe项目方）以外，都无法发布Platform任务（即公域任务）。Community任务为本社区的社区任务，受众为社区内的所有用户。

Condition处可以选择1\~N个条件，目前类型分为四大类：关注、点赞、转推、评论。后方输入框内输入需要关注的推特账户链接或点赞、转推、评论的推文链接。

Points处输入本次任务的奖励金额任务设置成功后，点击Confirm进行立即发布

<figure><img src="https://xyp4jzgb7d.feishu.cn/space/api/box/stream/download/asynccode/?code=ZjBhYmYxODkxYjI1ODJmZjgwZmI5NjYyMDc5OTQyZTZfZm9ZMzhOeGowQUJDMDJrVG9HcE0yYlE4TFZDY1VqeERfVG9rZW46Ym94Y241YUNhNVduWDJwVnVxQXBlZnFJeE11XzE2NzAzMzg5MDM6MTY3MDM0MjUwM19WNA" alt=""><figcaption></figcaption></figure>
