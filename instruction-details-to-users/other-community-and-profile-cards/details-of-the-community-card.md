# Details of the Community Card

<figure><img src="https://lh5.googleusercontent.com/cw40tYMizoPRPK7SskzmRzD9FfZ4rHE_ngBQhisZhYi46sDImn0XhO3HIvmk5ssk-Mm8cyn6QaxPFlv9HUTpbK-odbG99rL2wx-v9v_e-wg5N8AIo-s35ZD8fXHNF08-hcRPqV5kDXAjVw6Ge54G8RaNWEErLoXJY6PaIelujpU043kopEgNsc4dEOid" alt=""><figcaption></figcaption></figure>

1.  **General Community Information**

    General Community Information includes：

    1. Community Logo/Avatar&#x20;
    2. Community Name&#x20;
    3. Community Profile&#x20;
    4. Links relative to the community: website, opensea, discord, twitterscan, element, etc.
2.  **Members**

    This section displays the total number of community users who are currently using the plugin and the number of new users added today.&#x20;

    Click on “All Members” to view a list of the entire community in the pop-up window, and follow all of them with the “Follow All” button. This pop-up page also displays the community points and rankings of all users in your community.
3.  **Notification**

    Current community notices are shown here.
4.  **Community Tasks**

    Current active tasks for the community.
5. **Other Profile Cards**
   1. Clicking on another user's avatar in the members list will send you to the other users' Twitter and profile card.&#x20;
   2. Directly searching the user on Twitter allows you to view their profile card.

<figure><img src="https://lh5.googleusercontent.com/T7YV_x_9Cd0Em0K6Zg-0WJ5DbZNEKM0ugbHn1eskPIwY5yd-VMpvePaxx4f1gXTM2C8WQB-scwd2h4OVIhsQdn-zj4ANdUBVt2C_eHQLoXTyTNnVHK6rTowdw2ejB8TFEu4hkLnUFA8BCyB5XxMa32cRG3wCFW_1LQUiYHZD7iJVMKCalcoxJBNLW-Qc" alt=""><figcaption></figcaption></figure>
