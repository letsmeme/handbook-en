# Details of the Task Card

<figure><img src="https://lh4.googleusercontent.com/woxMlI-kkUT5ES6fi-ybloW8s9MYFt-jJ-6Xz2N9-5RCIMiBe7wvcn0l4SbR-CVW78AA3m5OV-GSBrhMhvb9Es2u9en7-9XO9WxILH-udIeCAio_iADEfbzn0yrIYsMLh00Skc71FeWwzFLUF2hZ5FT8koTIQhA4o0GU2go4sRizx03pOM8xzhq8vpoi" alt=""><figcaption></figcaption></figure>

1.  **Community Tasks**

    Community tasks are private tasks published by the community that are only available to users within that community. The default reward is distributed in that community’s native currency. However the task creator can choose to distribute MEME points instead.
2.  **MeMe Tasks**

    The MeMe task is a public task open to the entire Let's MeMe community. Other projects can submit requests for Meme tasks along with the $MEME fee in order for the official Let’s Meme team to publish the task. End users will receive $MEME for completion of MeMe tasks.
3.  **Community Info**

    Clicking on the community avatar will take you to the community home page.
4.  **How to Complete and Submit Tasks**

    There are four main categories of tasks:

    1. Follow: Users must follow a certain account.&#x20;
    2. Like: Users must like a tweet.&#x20;
    3. Retweet: Users must retweet a certain tweet.&#x20;
    4. Comment: Users must comment on a specific tweet. A single task if made up of any combination of the above 4 categories.

    After completing the steps and hitting the submit button, the button will display as “Pending Verification”, indicating that the submission was successful and is awaiting review. Once the task is verified as having been fully completed, your compensation will be rewarded.



****
