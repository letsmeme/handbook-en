# MeMe and Community Tasks

<figure><img src="https://lh5.googleusercontent.com/cEz1OCGKx9jvtVEF1WOPx2ThxgP9FRxs5oCHlCC5qetRuwL9k1NiEmxKsxoBDRoxzFZSHgtUPOuD27Y94w75WesPFyFuNHT_AIi7oO6lYKNPohj1unfoQJHT_dJ6dS4NVBEPVvTm8fEF7gTfcnbh5Tq9pmwivqXNsaNd-Y2at7WaWzwLafiQtlzuCaaP" alt=""><figcaption><p><strong>After connecting your wallet, the community tasks and MeMe tasks can be viewed on the Twitter Home page.</strong></p></figcaption></figure>

****
