# Download and Update the Extension

**Currently, the plug-in extension can be downloaded from the Chrome extension store.Download link:** [**https://chrome.google.com/webstore/detail/let%E2%80%99s-meme/fhplcfaddamamojjdgblhjbfneadoold/related**](https://chrome.google.com/webstore/detail/let%E2%80%99s-meme/fhplcfaddamamojjdgblhjbfneadoold/related)



\
**Let's MeMe Activating and Connecting**

<figure><img src="https://lh6.googleusercontent.com/AtzT6zkQYuDzta9XNxfRlLESKQtiErvyYzYI1_5JcmwnuYJ8q1CVa2Wa-CHICyShwX-HKRQScZTB_jDteRXE0lRF8eKYHZiXScUsLuu7TcOAA2euZZMkZ6Fybz5o__HykzpuQNITY3sB9mpMzTP1sIqpDmd5fDPIa2YzeormoE-i7_AQSrhWI4RFQtEg" alt=""><figcaption></figcaption></figure>

First, log in to the Twitter account you need to use and bind. After logging in to Twitter with the extension installed, the plugin will appear in the upper right corner of the webpage. Click to connect your wallet and approve the signature to start your meme journey.\
Note: The plugin only obtains the signature for the wallet address id and does not request access for anything related to your assets so you can rest assured.\
\
