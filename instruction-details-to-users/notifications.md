# Notifications

<figure><img src="https://lh4.googleusercontent.com/S3DO3QDEMWTUr6xcEyr0FMwffQtdGIGNDNkeRjZ9eyrUQ4nfKldSNJWmqRVLaVvh_5y8tOhV8b41V1IdfUofzCp9cAY4TOOkFVYmDhnzmrM59lO2t07uLdW6sN9vonmpmIPKBI42LYK1VI3U2IwQkHXtvl5EJcCrzVh0CKORBYTJxABsq9FtMjxZtDqN" alt=""><figcaption><p>Notification Card</p></figcaption></figure>

**With your wallet connected, heading to the Notifications page on Twitter allows you to view all of your different communities’ notifications.**\
