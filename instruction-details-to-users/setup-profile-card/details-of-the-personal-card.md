# Details of the Personal Card

<figure><img src="https://lh5.googleusercontent.com/L5ArhJJCf2dJ5S1sw1juh7VoqBZpagiY_7VyXMi4Ns-XsEnVq84TrX3Wdko12dFkNIqx7T-aMsm1TMeKCbN8dANY1x354ASRw2NlJws7doYM6NqPP3VGzXoY1bDp3Bn4XGHUSdHxnlWhlaABKWzuspWYUvmyINVIfOcwfPoZLHBlMZX8yh03QmHzcBYW" alt=""><figcaption></figcaption></figure>

****

1. **Basic Personal Information:** At the top of the profile card is the personal information section, which includes four modules: avatar, nickname, bio, and tags. You can click the Settings button to edit them.When using an NFT in the wallet as the avatar, the collection and number of the NFT will automatically be displayed under the nickname.
2. **Communities:** This section displays the communities of the NFT collections that the user wallet holds. Click the settings button to enter the community management page, where you can display or hide the community you are a part of.Once the community list is launched, the communities will be automatically sorted by community ranking.
3. **Music:** This feature is currently under the closed beta stage and is only available to TCG community members. Click the button to play/pause personalized music. The current TCG community setting is the TCG Song.
4. **Assets:** SectionThe asset section will display your current $MEME points as well as other communities’ currencies.
5. **Unbind wallet:** Click to unbind your Twitter from your wallet. Personal information and assets will remain with the wallet address and not Twitter.
6. **Version Information and Upgrade Reminder:** The current plug-in version is v1.1.8. A red light indicates that there is an updated version available.
7. **Personal Info Settings Page:** This page appears after clicking the Personal Information Settings button. Click on the avatar to set a personal avatar. NFTs are encouraged for avatars and using one will display the collection info in the basic profile information section. Nickname is the user’s nickname. Bio is the user’s introduction. Traits are label tags that can be included or hidden. If there are many labels, it will appear as a scroll wheel. Finally, click Save Changes to save the settings

\


<figure><img src="https://lh3.googleusercontent.com/OnV1PArJEo7oQtUULuD9kuKKpExVaLTsxPlPJ31U0fy1wQ1uvFYXAsIFFynJYYtjP0izp3Q70_5y3YiB7cf0ds9ldEQGzrJrxWCbEKSDtQaFtXIlmO21TnP7yGqUOxHODcOjdUUsQDQJ37Kvk2dcuKAuWD37uSIU9A2UrFRIBn9l1TiFWCTwp3sDd-AN" alt=""><figcaption></figcaption></figure>
