# Setup Profile Card

\
After activating the plugin, you are able to set up and modify your profile card.The purpose of the profile cards is to provide a web 3 profile on Twitter for other Let’s Meme users to access. Viewing other profile cards will serve as your introduction to others users in the web 3 space, and likewise, other users can gain an insight into your character and your web 3 presence from your profile.

<figure><img src="https://lh5.googleusercontent.com/O4_BsuxJk_1vH84UsAIwLX73SR6W6NvbYLiotSR6Hu5rrf-HbeuaFg0ppUv8WlaR14tkisORaoJjMIH03jteiUggjRJ2YiLtDvFYyzuQvEy8rsMl5XzzzS5ZFnsA1rdYfP0TBBgsRB48tA1WNosJxNp0EiY3L7crrTrXrGegOUKIrKCHK4DSTQbFjZwK" alt=""><figcaption></figcaption></figure>
